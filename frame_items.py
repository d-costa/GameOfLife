from functools import partial
from sys import version_info

from blueprints import Blueprints

if version_info > (3, 0):
    import tkinter as tk
else:
    import Tkinter as tk


class FrameItems(object):

    def __init__(self, root, bg, width, height):
        """Initiates the object MyFrame

        :param root: the parent to use to attach the frame
        :param bg: background of the object
        :param width: width of the frame
        :param height: height of the frame
        """
        self.root = root
        self.width = width
        self.height = height
        self.bg = bg
        self.selected_blueprint = None
        self.is_fliph = False
        self.is_flipv = False
        self.frame = tk.Frame(root, padx=5, pady=10, bg=bg, width=width, height=height)
        self.buttons = []
        self.fliph_button = tk.Button(master=self.frame, text="Flip H", anchor='c',
                                      relief='flat',
                                      pady=30, command=self.fliph)
        self.flipv_button = tk.Button(master=self.frame, text="Flip V", anchor='c',
                                      relief='flat',
                                      pady=30, command=self.flipv)
        self.create_buttons()

    def pack(self, side):
        """Packs the frame and its children.

        :param side: top, bottom, left or right.
        :return: None
        """
        self.frame.pack(side=side, fill='both', expand=True)
        for button in self.buttons:
            button.pack(side='top', expand=True, fill='both')
        self.fliph_button.pack(side='left', expand=False, fill=None)
        self.flipv_button.pack(side='right', expand=False, fill=None)

    def create_buttons(self):
        for blueprint_name in Blueprints.blueprints_map.keys():
            self.buttons.append(
                tk.Button(master=self.frame, text=blueprint_name,
                          command=partial(self.select_blueprint, blueprint_name),
                          anchor='c',
                          relief='flat', pady=10))

    def select_blueprint(self, name):
        if self.selected_blueprint == Blueprints.blueprints_map[name]:
            self.selected_blueprint = None
            for button in self.buttons:
                button["relief"] = "flat"
            return
        self.selected_blueprint = Blueprints.blueprints_map[name]
        for button in self.buttons:
            if button["text"] == name:
                button["relief"] = "sunken"
            else:
                button["relief"] = "flat"

    def has_selected_blueprint(self):
        return self.selected_blueprint is not None

    def get_selected_blueprint(self):
        blueprint = self.selected_blueprint
        if self.is_fliph:
            blueprint = Blueprints.flip_horizontal(blueprint)
        if self.is_flipv:
            blueprint = Blueprints.flip_vertical(blueprint)
        return blueprint

    def fliph(self):
        self.is_fliph = not self.is_fliph
        if self.is_fliph:
            self.fliph_button['relief'] = "sunken"
        else:
            self.fliph_button['relief'] = "flat"

    def flipv(self):
        self.is_flipv = not self.is_flipv
        if self.is_flipv:
            self.flipv_button['relief'] = "sunken"
        else:
            self.flipv_button['relief'] = "flat"

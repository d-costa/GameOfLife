# GameOfLife
Simple implementation of Conway's Game of Life with Python, using Tkinter.
Uses python 3.  

## Usage
Run `python main.py`  to launch.

Use the buttons on the left to select an object, press `Flip H` or `Flip V` to flip the object horizontally and/or vertically, 
and finally press the screen to position the object.  

To start the simulation, press the `Start / Pause` button.  

![Screenshot](media/screenshot.png)

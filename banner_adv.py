from sys import version_info

if version_info > (3, 0):
    import tkinter as tk
else:
    import Tkinter as tk

from sys import maxsize
from banner import Banner

"""This is a frame with a label as title, another label as a counter, one keeping track of max and another of min.
"""


class BannerAdv(Banner):
    """Inherits Banner.

        Creates a more advanced object with a title, a counter and max/min records, inside a frame.

        The frame is set to fill ***x*** and the title, the counter, max and min records are set to fill ***both***.
    """
    def __init__(self, root, title, counter_text, bg, width, height):
        """Initiates the object MyFrameAdv

        :param root: the parent to use to attach the frame
        :param title: title of the frame
        :param counter_text: initial text of the counter
        :param bg: background of the object
        :param width: width of the frame
        :param height: height of the frame
        """
        super(BannerAdv, self).__init__(root, title, counter_text, bg, width, height)
        self.min = maxsize
        self.max = - maxsize
        self.max_label = tk.Label(self.frame, text='Max: 0', bg=bg)
        self.min_label = tk.Label(self.frame, text='Min: 0', bg=bg)
        self.max_label.pack(side='top', fill='both', expand=1)
        self.min_label.pack(side='top', fill='both', expand=1)

    def pack(self, side):
        """Packs the frame and its children. The frame does not resize.

            :param side: top, bottom, left or right.
            :return: None
        """
        super(BannerAdv, self).pack(side)

    def set_text(self, num):
        """Substitutes the text of the counter with ***num***, and updates max and min records.

        :param num: str or int
        :return: None
        """
        super(BannerAdv, self).set_text(num)
        if num > self.max:
            self.max = num
        if num < self.min:
            self.min = num
        self.max_label.config(text='Max: ' + str(self.max))
        self.min_label.config(text='Min: ' + str(self.min))

    def reset(self):
        super().reset()
        self.min = maxsize
        self.max = - maxsize
        self.max_label.config(text='Max: ' + str(0))
        self.min_label.config(text='Min: ' + str(0))

import copy

"""Game of Life
"""


class Game(object):

    def __init__(self, width, height):
        """Inits the game.
        """
        self.width = width
        self.height = height
        self.alive_cells = set()
        self.num_generation = 0
        self.paused = True
        self.num_alive_cells = 0

    def get_num_neighbors(self, cell):
        """Returns the number of alive cells surrounding **cell**.
        :return: int
        """
        x = cell[0]
        y = cell[1]
        positions = [(x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
                     (x - 1, y), (x + 1, y),
                     (x - 1, y + 1), (x, y + 1), (x + 1, y + 1)]
        counter = 0
        for pos in positions:
            if pos in self.alive_cells and self.width > pos[0] > -1 and self.height > pos[1] > -1:
                counter += 1

        return counter

    def get_proximal_cells(self, cell):
        """Returns a set with the surrounding cells of ***cell***
        :return: set
        """
        x = cell[0]
        y = cell[1]
        positions = [(x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
                     (x - 1, y), (x + 1, y),
                     (x - 1, y + 1), (x, y + 1), (x + 1, y + 1)]
        neighbors = set()
        for pos in positions:
            if self.width > x > -1 and self.height > y > -1:
                neighbors.add(pos)

        return neighbors

    def advance(self):
        """Only works when the game is not paused.
        Calculates and advances to the next generation.

        Loops over the cells surrounding the alive cells and checks for its neighbours.

        Increments the generation number.

        :return: None
        """
        if not self.paused:
            self.num_alive_cells = 0
            future_alive_cells = set()
            relevant_cells = copy.deepcopy(self.alive_cells)
            for pos in self.alive_cells:
                relevant_cells |= self.get_proximal_cells(pos)
            for pos in relevant_cells:
                num_neighbors = self.get_num_neighbors(pos)
                current_cell = 0
                if pos in self.alive_cells:
                    current_cell = 1
                if current_cell == 1:
                    if num_neighbors in (2, 3):
                        future_alive_cells.add(pos)
                        self.num_alive_cells += 1
                elif current_cell == 0:
                    if num_neighbors == 3:
                        future_alive_cells.add(pos)
                        self.num_alive_cells += 1
            self.alive_cells = future_alive_cells
            self.num_generation += 1

    def create_object(self, blueprint, initial_pos):
        """Adds the list of coordinates of ***blueprint to the alive cells.
        :return: None
        """
        for pos in blueprint:
            x = pos[0] + initial_pos[0]
            y = pos[1] + initial_pos[1]
            if x <= self.width and y <= self.height:
                self.alive_cells.add((x, y))
                self.num_alive_cells += 1

    def toggle(self):
        """Pauses or unpauses the game
        :return: None
        """
        self.paused = not self.paused

    def reset(self):
        self.alive_cells = set()
        self.num_generation = 0
        self.paused = True
        self.num_alive_cells = 0

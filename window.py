from sys import version_info

from banner import Banner
from banner_adv import BannerAdv
from frame_counter import FrameCounter
from frame_items import FrameItems
from game import Game

if version_info > (3, 0):
    import tkinter as tk


class Window(object):
    min_sleep_time = 10
    max_sleep_time = 1000
    default_sleep_time = 100

    def __init__(self, width, height):
        self.sleep_time = self.default_sleep_time
        self.sleep_time_hover = 10
        self.cell_size = 5
        self.cell_interval = 2
        self.window_width = width
        self.window_height = height
        self.hovering = False
        self.mouse_x = 0
        self.mouse_y = 0

        # root
        self.root = tk.Tk()

        # self.root.resizable(width=False, height=False)
        self.root.geometry('{}x{}'.format(width, height))
        self.root.title('Conway\'s Game of Life')

        # measures
        self.bottom_panel_height = 0
        self.canvas_width = int(width * 8 / 10)
        self.canvas_height = self.window_height
        self.side_panel_width = int(width * (1 / 10))  # two panels
        self.max_x = int(self.canvas_width / self.cell_size)
        self.max_y = int(self.canvas_height / self.cell_size)

        # canvas / game
        self.canvas = tk.Canvas(master=self.root, width=self.canvas_width, height=self.canvas_height, bg='black')
        self.canvas.bind("<Button-1>", self.callback)
        self.canvas.bind("<Enter>", self.on_enter)
        self.canvas.bind("<Leave>", self.on_leave)
        self.canvas.bind("<Motion>", self.motion)
        self.game = Game(width=self.max_x, height=self.max_y)

        # panels
        # this panel will have the labels and the buttons
        self.right_panel = tk.Frame(self.root, padx=5, width=self.side_panel_width, height=self.canvas_height)
        self.right_panel.propagate(False)
        # this panel will have the items
        self.left_panel = tk.Frame(self.root, padx=5, width=self.side_panel_width, height=self.canvas_height)
        self.left_panel.propagate(False)

        # child frames
        self.current_gen_frame = Banner(self.right_panel, 'Generation', 0, '#d9d9d9', self.side_panel_width, 60)
        self.cells_alive_frame = BannerAdv(self.right_panel, 'Alive cells:', 0, '#d9d9d9', self.side_panel_width, 60)
        self.gps_frame = BannerAdv(self.right_panel, 'Gen Per Second:', '0', '#d9d9d9', self.side_panel_width,
                                   60)  # generations per second
        self.items_frame = FrameItems(self.left_panel, '#d9d9d9', self.side_panel_width, self.canvas_height)
        self.items_frame.pack('top')
        self.pack_banners()
        self.create_right_buttons()
        self.frame_counter = FrameCounter()

    def _ruler(self):
        for i in range(0, int(self.canvas_width / 5), 10):
            self.canvas.create_text(i * 5, 50, fill='green', font='Times 8', text=i)
        for i in range(0, int(self.canvas_height / 5), 10):
            self.canvas.create_text(50, i * 5, fill='green', font='Times 8', text=i)

    def pack_banners(self):
        self.left_panel.pack(side='left', expand=True, fill=None)

        self.canvas.pack(side='left', expand=True, fill=None)

        self.right_panel.pack(side='left', expand=True)

        self.current_gen_frame.pack('top')
        self.cells_alive_frame.pack('top')
        self.gps_frame.pack('top')
        spacer = tk.Frame(self.right_panel, width=self.side_panel_width, height=200)
        spacer.pack(side='top')

    def add_point(self, x, y, tags='cell', fill="white"):
        self.canvas.create_rectangle(x, y, x + self.cell_size, y + self.cell_size, width=self.cell_interval,
                                     fill=fill, tags=tags)

    def clear_canvas(self):
        self.canvas.delete('cell')

    def print_cells(self):
        self.clear_canvas()
        for cell in self.game.alive_cells:
            self.add_point(cell[0] * self.cell_size, cell[1] * self.cell_size)

    def create_right_buttons(self):
        self.pause_button = tk.Button(master=self.right_panel, text="Start / Pause", command=self.start_pause,
                                      anchor='c',
                                      pady=15, relief='raised')
        self.pause_button.pack(side='top', expand=True, fill='both')

        dec_sleep_button = tk.Button(self.right_panel, text="Faster", command=self.dec_sleep_time, anchor='c', pady=15,
                                     relief='flat')
        dec_sleep_button.pack(side='top', expand=True, fill='both')

        inc_sleep_button = tk.Button(self.right_panel, text="Slower", command=self.inc_sleep_time, anchor='c', pady=15,
                                     relief='flat')
        inc_sleep_button.pack(side='top', expand=True, fill='both')

        reset_button = tk.Button(self.right_panel, text="Reset", command=self.reset, anchor='c', pady=15,
                                 relief='flat')
        reset_button.pack(side='top', expand=True, fill='both')

    def inc_sleep_time(self):
        if self.sleep_time < Window.max_sleep_time:
            self.sleep_time = int(round(self.sleep_time * 1.5))
        if self.sleep_time > Window.max_sleep_time:
            self.sleep_time = Window.max_sleep_time

    def dec_sleep_time(self):
        if self.sleep_time > Window.min_sleep_time:
            self.sleep_time = int(round(self.sleep_time / 1.5))
        if self.sleep_time < Window.min_sleep_time:
            self.sleep_time = Window.min_sleep_time

    def update_labels(self):
        self.current_gen_frame.set_text(self.game.num_generation)
        self.cells_alive_frame.set_text(self.game.num_alive_cells)

    def callback(self, event):
        # left mouse click on canvas
        if event.num == 1:
            if self.items_frame.has_selected_blueprint():
                x = self.cell_size * (event.x // 5)
                y = self.cell_size * (event.y // 5)
                blueprint = self.items_frame.get_selected_blueprint()
                self.game.create_object(blueprint,
                                        [x / self.cell_size, y / self.cell_size])

    def start_pause(self):
        self.game.toggle()
        if self.game.paused:
            self.pause_button["relief"] = "raised"
        else:
            self.pause_button["relief"] = "sunken"

    def reset(self):
        self.game.reset()
        self.current_gen_frame.reset()
        self.cells_alive_frame.reset()
        self.gps_frame.reset()
        self.frame_counter = FrameCounter()
        self.pause_button["relief"] = "raised"
        self.sleep_time = self.default_sleep_time

    def on_enter(self, event):
        self.hovering = True

    def on_leave(self, event):
        self.hovering = False

    def motion(self, event):
        self.mouse_x = event.x
        self.mouse_y = event.y

    def update_hover_object(self):
        self.canvas.delete("hover")
        if self.hovering and self.items_frame.has_selected_blueprint():
            for point in self.items_frame.get_selected_blueprint():
                self.add_point(self.mouse_x + point[0] * self.cell_size, self.mouse_y + point[1] * self.cell_size,
                               "hover", "gray")
        self.root.after(self.sleep_time_hover, self.update_hover_object)

    def update(self):
        self.game.advance()
        fps = self.frame_counter.tick()
        if not fps == -1:
            self.gps_frame.set_text(fps)
        self.print_cells()
        self.update_labels()
        self.root.after(self.sleep_time, self.update)

    def mainloop(self):
        self.root.after(self.sleep_time, self.update)
        self.root.after(self.sleep_time_hover, self.update_hover_object)
        self.frame_counter.start()
        self.root.mainloop()

import time
from sys import maxsize


class FrameCounter(object):
    """Counts the number of frames each second, keeping track of min and max values.
    """

    def __init__(self):
        """Inits the counter
        """
        self.frames_this_second = 0
        self.min_fps = maxsize
        self.max_fps = - maxsize
        self.initial_time = time.time()

    def tick(self):
        """
        If it passed more then 1 second since the last call of ***tick()***,
        returns the frames counted during that time, and resets the counter.

        Else, increments the frame counter and returns -1.
        """
        fps = self.frames_this_second
        result = -1
        if time.time() - self.initial_time > 1:
            if self.frames_this_second > self.max_fps:
                self.max_fps = self.frames_this_second
            if self.frames_this_second < self.min_fps:
                self.min_fps = self.frames_this_second
            self.initial_time = time.time()
            self.frames_this_second = 0
            result = fps
        self.frames_this_second += 1
        return result

    def start(self):
        """Starts counting the time."""
        self.initial_time = time.time()

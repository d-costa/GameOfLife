from sys import version_info

if version_info > (3, 0):
    import tkinter as tk
else:
    import Tkinter as tk

"""This is a frame with a label as title and another label as a counter.
"""


class Banner(object):
    """Creates a simple object with a title and a counter inside a frame.
        The frame is set to fill ***x*** and both the title and the counter are set to fill ***both***.

    """

    def __init__(self, root, title, counter_text, bg, width, height):
        """Initiates the object MyFrame

        :param root: the parent to use to attach the frame
        :param title: title of the frame
        :param counter_text: initial text of the counter
        :param bg: background of the object
        :param width: width of the frame
        :param height: height of the frame
        """
        self.root = root
        self.width = width
        self.height = height
        self.frame = tk.Frame(root, padx=5, pady=10, bg=bg, width=width, height=height)
        self.title_label = tk.Label(master=self.frame, text=title, bg=bg, font=('Helvetica', 10, 'bold'))
        self.counter_label = tk.Label(master=self.frame, text='Current: ' + str(counter_text), bg=bg)

        self.title_label.pack(side='top', fill='both', expand=True)
        self.counter_label.pack(side='top', fill='both', expand=True)

    def pack(self, side):
        """Packs the frame and its children. The frame does not resize.

        :param side: top, bottom, left or right.
        :return: None
        """
        self.frame.pack(side=side, fill='both', expand=True)

    def set_text(self, num):
        """Substitutes the text of the counter with ***num***.

        :param num: str or int
        :return: None
        """
        self.counter_label.config(text='Current: ' + str(num))

    def reset(self):
        self.counter_label["text"] = 0

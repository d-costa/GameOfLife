from sys import version_info

from blueprints import Blueprints as Bp
from window import Window

window_width = 1360
window_height = 720


def create_objects(window, game):
    glider_gun_width = Bp.width(Bp.GLIDER_GUN)
    glider_gun_height = Bp.height(Bp.GLIDER_GUN)

    # game.create_object(Bp.GLIDER_GUN, (42, 10))
    #
    # game.create_object(Bp.flip_vertical(Bp.GLIDER_GUN), (42, game.height - glider_gun_height - 10))
    #
    # game.create_object(Bp.flip_horizontal(Bp.GLIDER_GUN), (game.width - glider_gun_width - 42, 10))
    #
    # game.create_object(Bp.flip_vertical(Bp.flip_horizontal(Bp.GLIDER_GUN)),
    #                    (game.width - glider_gun_width - 42, game.height - glider_gun_height - 10))


def main():
    if version_info < (3, 0):
        print("Please use python3. Exiting...")
        return
    window = Window(width=window_width, height=window_height)
    window.mainloop()


if __name__ == '__main__':
    main()

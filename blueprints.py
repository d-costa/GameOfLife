from sys import maxsize


class Blueprints(object):
    """Each lists contains the coordinates of each live cell needed to create the figure.
    """
    BLINKER = [[0, 0], [1, 0], [2, 0]]

    GLIDER = [[1, 0], [2, 1], [0, 2], [1, 2], [2, 2]]

    TOAD = [[1, 0], [2, 0], [3, 0], [0, 1], [1, 1], [2, 1]]

    LWSS = [[1, 0], [4, 0], [0, 1], [0, 2], [4, 2], [0, 3], [1, 3], [2, 3], [3, 3]]

    BLOCK = [[0, 0], [1, 0], [0, 1], [1, 1]]

    TARGET = [[0, 0], [1, 0], [2, 0], [0, 1], [1, 1], [2, 1], [0, 2], [1, 2], [2, 2]]

    BEACON = [[0, 0], [0, 1], [1, 0], [2, 3], [3, 2], [3, 3]]

    BOAT = [[0, 0], [0, 1], [1, 0], [1, 2], [2, 1]]

    SNAKE = [[0, 0], [0, 1], [1, 1], [2, 0], [3, 0], [3, 1]]

    # PUFFER = [[3, 2], [4, 2], [5, 2], [6, 2], [3, 3], [4, 3], [5, 3], [6, 3], [16, 2], [16, 3],
    #           [17, 2], [17, 3], [18, 1], [18, 4], [19, 0], [19, 5], [20, 0], [20, 1], [20, 4], [20, 5],
    #           [21, 1], [21, 4], [32, 1], [32, 2], [32, 3], [32, 4], [33, 0], [33, 5]]  # ACTUALLY NOT PUFFER

    SPACESHIP = [[0, 1], [0, 2], [0, 3], [0, 7], [0, 8], [0, 9], [1, 0], [1, 3], [1, 7], [1, 10],
                 [2, 3], [2, 7], [3, 3], [3, 7], [4, 0], [4, 2], [4, 8], [4, 10], [6, 5], [6, 5],
                 [6, 6], [7, 4], [7, 5], [7, 6], [8, 4], [8, 6], [9, 5], [10, 5], [13, 3], [13, 7],
                 [14, 3], [14, 4], [14, 6], [14, 7], [15, 4], [15, 6], [17, 5], [18, 5], [19, 5]]

    PENTADECATHLON = [[0, 1], [1, 1], [2, 0], [2, 2], [3, 1], [4, 1], [5, 1], [6, 1], [7, 0], [7, 2],
                      [8, 1], [9, 1]]

    GLIDER_GUN = [[0, 4], [0, 5], [1, 4], [1, 5], [10, 4], [10, 5], [10, 6], [11, 3], [11, 7], [12, 2],
                  [12, 8], [13, 2], [13, 8], [14, 5], [15, 3], [15, 7], [16, 4], [16, 5], [16, 6], [17, 5],
                  [20, 2], [20, 3], [20, 4], [21, 2], [21, 3], [21, 4], [22, 1], [22, 5], [24, 0], [24, 1],
                  [24, 5], [24, 6], [34, 2], [34, 3], [35, 2], [35, 3]]

    FIGURE_EIGHT = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2], [3, 3], [3, 4], [3, 5],
                    [4, 3], [4, 4], [4, 5], [5, 3], [5, 4], [5, 5]]

    RPENTOMINO = [[0, 1], [0, 2], [1, 0], [1, 1], [2, 1]]

    blueprints_map = {"Blinker": BLINKER, "Glider": GLIDER, "Toad": TOAD, "LWSS": LWSS, "Block": BLOCK,
                      "Target": TARGET, "Beacon": BEACON, "Boat": BOAT, "Snake": SNAKE, "Spaceship": SPACESHIP,
                      "Pentadecathlon": PENTADECATHLON, "Glider Gun": GLIDER_GUN,
                      "Figure Eight": FIGURE_EIGHT, "R-Pentomino": RPENTOMINO}

    @staticmethod
    def get_center(blueprint):
        """Returns the center of the figure as a tuple of integers,(x, y).
        :param blueprint: the figure
        :return: the center of the figure as (x, y)
        """
        x_size = max(list(pos[0] for pos in blueprint)) - min(list(pos[0] for pos in blueprint))
        y_size = max(list(pos[1] for pos in blueprint)) - min(list(pos[1] for pos in blueprint))
        return int(x_size / 2), int(y_size / 2)

    @staticmethod
    def flip_horizontal(blueprint):
        """Returns another list of coordinates but flipped on the x axis.
        :param blueprint: the figure
        :return: a new list of coordinates to draw a flipped ***blueprint***
        """
        res = []
        x_size = max(list(pos[0] for pos in blueprint)) - min(list(pos[0] for pos in blueprint))
        right_most = min(list(pos[0] for pos in blueprint))
        for pos in blueprint:
            old_x = pos[0]
            new_x = right_most - old_x - 1  # flip
            new_x += x_size  # reposition

            res.append([new_x, pos[1]])

        return res

    @staticmethod
    def flip_vertical(blueprint):
        """Returns another list of coordinates but flipped on the y axis.
        :param blueprint: the figure
        :return: a new list of coordinates to draw a flipped ***blueprint***
        """
        res = []
        y_size = max(list(pos[1] for pos in blueprint)) - min(list(pos[1] for pos in blueprint))
        top_most = min(list(pos[1] for pos in blueprint))
        for pos in blueprint:
            old_y = pos[1]
            new_y = top_most - old_y - 1  # flip
            new_y += y_size  # reposition

            res.append([pos[0], new_y])

        return res

    @staticmethod
    def width(blueprint):
        """Returns the center of the figure as a tuple of integers,(x, y).
        :param blueprint: the figure
        :return: the width of the figure as an integer
        """
        min_x = maxsize
        max_x = - maxsize
        for pos in blueprint:
            if pos[0] < min_x:
                min_x = pos[0]
            if pos[0] > max_x:
                max_x = pos[0]
        return max_x - min_x

    @staticmethod
    def height(blueprint):
        """Returns the height of the figure.
        :param blueprint: the figure
        :return: the height of the figure as an integer
        """
        min_y = maxsize
        max_y = - maxsize
        for pos in blueprint:
            if pos[1] < min_y:
                min_y = pos[1]
            if pos[1] > max_y:
                max_y = pos[1]
        return max_y - min_y
